PI=pi@192.168.2.2
PYPY=/home/pi/pypy-2.3-linux-armhf-raspbian/bin/pypy
rm -rf dist
python2.7 setup.py sdist
scp dist/*.tar.gz $PI:/tmp
scp conf/sprungkraft.conf $PI:
scp demo-data/*.* $PI:/tmp
ssh $PI "rm -rf /home/pi/Sprungkraft*; tar xf /tmp/Sprung*.tar.gz"
ssh $PI "cd Sprungkraft*; $PYPY setup.py develop"
ssh $PI "rm -rf /home/pi/sprungkraft-assets; mkdir /home/pi/sprungkraft-assets"
scp sprungkraft/assets/* $PI:sprungkraft-assets/


