#
from unittest import TestCase

from sprungkraft.util import Line, Rect, RectClassifier, DistanceMeasurer

from .base import load_data

class TestGeometry(TestCase):


    def test_rect_contains(self):
        r = Rect((10, 10), (20, 20))
        assert (9, 9) not in r
        assert (10, 10) in r
        assert (10, 30) not in r
        assert (10, 29) in r
        assert (30, 10) not in r
        assert (29, 10) in r


    def test_rect_lines(self):
        r = Rect((10, 20), (30, 40))
        self.assertEqual(
            Line((10, 20), (30, 0)),
            r.bottom_line,
            )
        self.assertEqual(
            Line((10, 60), (30, 0)),
            r.top_line,
            )
        self.assertEqual(
            Line((10, 20), (0, 40)),
            r.left_line,
            )
        self.assertEqual(
            Line((40, 20), (0, 40)),
            r.right_line,
            )


    def test_line_equivalence(self):
        a = Line((10, 20), (20, 0))
        b = Line((30, 20), (-20, 0))
        self.assertEqual(a, b)
        c = Line((10, 20), (0, 20))
        d = Line((10, 40), (0, -20))
        self.assertEqual(c, d)


    def test_line_distance(self):
        l = Line((10, 20), (30, 0))
        self.assertEqual(
            10,
            l.distance((10, 30)),
            )


    def test_fast_clip(self):
        points = [(-5.0, 1.0), (-1.0, 1.0), (4.0, 6.0)]
        rect = Rect((-2.0, 0), (5.0, 3.0))
        self.assertEqual(
            [(-1.0, 1.0)],
            rect.fast_clip(points),
            )


    def test_fast_clip_with_real_data(self):
        points = load_data("points.json")
        rect = Rect((0, 0), (3.0, 1.034))
        filtered = rect.fast_clip(points)
        self.assertEqual(
            10,
            len(filtered),
            )
        import pprint
        pprint.pprint(filtered)


    def test_rect_classifier(self):
        cl = RectClassifier()
        self.assertEqual(
            frozenset(),
            cl.classify([]),
            )

        cl.add_classifier("start_area", Rect((0, 0), (0.48, 1.034)))

        self.assertEqual(
            frozenset(["start_area"]),
            cl.classify([(0.2, .5)]),
            )
        self.assertEqual(
            frozenset(),
            cl.classify([(0.5, .5)]),
            )

        cl.add_classifier("landing_area", Rect((0.48, 0), (3.0 - 0.48, 1.034)))
        self.assertEqual(
            frozenset(["landing_area", "start_area"]),
            cl.classify([(0.5, .5), (0.2, .5)]),
            )


    def test_distance_measurer(self):
        r = Rect((0, 0), (20,20))
        dm = DistanceMeasurer(r, r.left_line)
        assert dm.measurement is None
        dm.measure([(-2, 0)])
        assert dm.measurement is None
        dm.measure([(2, 1)])
        self.assertEqual(2, dm.measurement)
        dm.reset()
        assert dm.measurement is None
