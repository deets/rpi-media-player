from datetime import timedelta
from unittest import TestCase

from sprungkraft.predicates import (
    less_than_timedelta,
    greater_than_timedelta,
    subset,
    equal,
    contains,
    intersect,
    )




class TestPredicates(TestCase):

    def test_less_than_timedelta(self):
        p = less_than_timedelta(timedelta(seconds=10))
        assert p(timedelta(seconds=9))
        assert not p(timedelta(seconds=11))
        self.assertEqual(
            "< 10.00s",
            str(p),
            )


    def test_greater_than_timedelta(self):
        p = greater_than_timedelta(timedelta(seconds=10))
        assert not p(timedelta(seconds=9))
        assert p(timedelta(seconds=11))
        self.assertEqual(
            "> 10.00s",
            str(p),
            )


    def test_subset(self):
        p = subset({"a", "b"})
        assert p(None, {"a"})
        assert not p(None, {"c"})
        self.assertEqual(
            "in(a, b)",
            str(p),
            )


    def test_equal(self):
        p = equal({"a", "b"})
        assert not p(None, {"a"})
        assert p(None, {"a", "b"})
        self.assertEqual(
            "=={a, b}",
            str(p),
            )


    def test_contains(self):
        p = contains("foobar")
        assert not p(None, {"baz"})
        assert p(None, {"foobar"})
        self.assertEqual(
            "contains(foobar)",
            str(p),
            )


    def test_intersect(self):
        p = intersect({"a", "b"})
        assert not p(None, {"c"})
        assert p(None, {"a"})
        self.assertEqual(
            "intersect({a, b})",
            str(p),
            )
