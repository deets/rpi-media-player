from unittest import TestCase

from sprungkraft.automaton import (
    Automaton,
    UnknownStateError,
    NoToStateError,
    TimedAutomaton,
    )

class TestAutomaton(TestCase):


    def test_starting_state(self):
        a = Automaton("start")
        self.assertEqual(
            "start",
            a.state,
            )


    def test_adding_state(self):
        a = Automaton("start")
        a.add_state("next")
        assert "next" in a


    def test_add_transition(self):
        a = Automaton("start")
        self.assertRaises(
            UnknownStateError,
            lambda: a["foo"],
            )
        self.assertRaises(
            UnknownStateError,
            a["start"].to, "end"
            )

        a.add_state("end")

        self.assertRaises(
            NoToStateError,
            a["start"].on, None,
            )

        a["start"].to("end").on(lambda input_: True)

        a.feed(object())
        self.assertEqual(
            "end",
            a.state,
            )


    def test_signalling(self):
        a = Automaton("start")
        a.add_state("end")
        a["start"].to("end").on(lambda input_: True)

        signalled = []

        def slot(automaton, from_, to):
            assert automaton is a
            self.assertEqual(
                ("start", "end"),
                (from_, to),
                )
            signalled.append(True)

        a.connect(slot)
        a.feed(object())

        assert signalled


    def test_dot(self):
        def norm(s):
            return s.replace(" ", "").replace("\n", "");

        a = Automaton("start")
        self.assertEqual(
            norm('digraph {\n          start [style = bold];\n        }'),
            norm(a.dot()),
            )

        a.add_state("end")
        self.assertEqual(
            norm('digraph { start [style = bold]; end; }'),
            norm(a.dot()),
            )


        class predicate(object):

            def __call__(*args):
                return True

            def __str__(self):
                return "predicate"


        a["start"].to("start").on(predicate())
        a["start"].to("end").on(predicate())

        self.assertEqual(
            norm('digraph { start [style = bold]; end; start -> start [ label = "1:predicate" ]; start -> end [label = "2:predicate"];}'),
            norm(a.dot()),
            )




class TestTimedAutomaton(TestCase):


    def test_when_on_start_state(self):
        now = 1000
        ta = TimedAutomaton("start", lambda: now)
        self.assertEqual(
            now,
            ta.when,
            )


    def test_timediff_is_passed_to_transition_predicates(self):
        timegen = iter([1000, 1010])
        ta = TimedAutomaton("start", lambda: timegen.next())
        ta.add_state("end")

        def slot(automaton, _from, _to):
            self.assertEqual(
                1010,
                automaton.when,
                )

        ta.connect(slot)
        input_ = object()

        def predicate(dt, feed_input):
            self.assertEqual(10, dt)
            assert input_ is feed_input
            return True

        ta["start"].to("end").on(predicate)
        ta.feed(input_)
        self.assertEqual(
            "end",
            ta.state,
            )
        self.assertEqual(
            1010,
            ta.when,
            )
