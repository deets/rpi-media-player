import os
import json

def load_data(path):
    full_path = os.path.join(
        os.path.dirname(__file__),
        "data",
        path,
        )
    assert os.path.exists(full_path)
    with open(full_path) as inf:
        return json.load(inf)
