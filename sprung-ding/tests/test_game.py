import os
from unittest import TestCase
import subprocess
from datetime import datetime, timedelta

import mox

from sprungkraft.game import Game

from .base import load_data

DOT =None

for candidate in ("/usr/local/bin/dot", "/usr/bin/dot"):
    if os.path.exists(candidate):
        DOT = candidate
        break

class TestGame(TestCase):


    def test_print_automaton(self):
        dot = Game(None, datetime.now).automaton.dot()
        p = subprocess.Popen(
            [DOT, "-Tpng"],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            )
        out, _ = p.communicate(dot)
        print dot
        with open("/tmp/graph.png", "wb") as outf:
            outf.write(out)



    def test_ready_jump_ready(self):

        m = mox.Mox()
        slot = m.CreateMockAnything()
        slot(mox.IgnoreArg(), "waiting")
        slot(mox.IgnoreArg(), "ready")
        slot(mox.IgnoreArg(), "flying")
        slot(mox.IgnoreArg(), "landed", 1.3 - 0.485)
        slot(mox.IgnoreArg(), "waiting")
        m.ReplayAll()

        times_it = iter([
            0,
            6,
            7,
            8,
            11,
            26,
            ])

        def timesource():
            return timedelta(seconds=times_it.next())

        conf = load_data("sprungkraft.conf")
        game = Game(conf, timesource)
        game.connect(slot)

        game.feed([])
        game.feed([(.3, .3)])
        game.feed([])
        game.feed([(1.3, .3)])
        game.feed([])

        m.VerifyAll()
