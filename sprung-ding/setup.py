import sys
from setuptools import setup, find_packages
import glob, subprocess
import os


setup(
    name = "Sprungkraft",
    version = "1.0",
    packages = find_packages(),
    author = "Diez B. Roggisch",
    author_email = "deets@web.de",
    description = "A interactive plaything for measuring jump distances",
    license = "MIT",
    keywords = "python",
    include_package_data=True,
    entry_points= {
        'console_scripts' : [
            'sprungkraft = sprungkraft.main:main',
            ]},

    )
