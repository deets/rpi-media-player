from datetime import timedelta


class predicate(object):


    def __call__(self, *args):
        return self.implementation(*args)


    def implementation(self, *args):
        raise NotImplementedError



class less_than_timedelta(predicate):

    def __init__(self, threshold):
        self.threshold = threshold


    def implementation(self, dt, *args):
        return dt < self.threshold


    def __str__(self):
        return "< %.2fs" % self.threshold.total_seconds()


class greater_than_timedelta(predicate):

    def __init__(self, threshold):
        self.threshold = threshold


    def implementation(self, dt, *args):
        return dt > self.threshold


    def __str__(self):
        return "> %.2fs" % self.threshold.total_seconds()


class equal(predicate):

    def __init__(self, thing):
        self.thing = thing


    def implementation(self, _dt, arg):
        return self.thing == arg


    def __str__(self):
        if isinstance(self.thing, set):
            thing_str = "{%s}" % ", ".join(item for item in sorted(self.thing))
        else:
            thing_str = str(self.thing)

        return "==%s" % thing_str


class subset(predicate):

    def __init__(self, superset):
        self.superset = superset


    def implementation(self, _dt, arg):
        return self.superset.issuperset(arg)


    def __str__(self):
        return "in(%s)" % ", ".join(str(elem) for elem in sorted(self.superset))


class true(predicate):

    def implementation(self, *args):
        return True


    def __str__(self):
        return "true"

true = true()


class contains(predicate):

    def __init__(self, thing):
        self.thing = thing


    def implementation(self, _dt, candidate):
        return self.thing in candidate


    def __str__(self):
        return "contains(%s)" % self.thing


class intersect(predicate):

    def __init__(self, some_set):
        self.some_set = some_set


    def implementation(self, _dt, other_set):
        return bool(self.some_set.intersection(other_set))


    def __str__(self):
        return "intersect({%s})" % ", ".join(item for item in sorted(self.some_set))
