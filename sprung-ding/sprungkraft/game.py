#
import logging
from datetime import datetime, timedelta
from .automaton import TimedAutomaton
from .predicates import (
    less_than_timedelta,
    greater_than_timedelta,
    equal,
    true,
    contains,
    intersect,
    )

from .util import RectClassifier, Rect, DistanceMeasurer

logger = logging.getLogger(__name__)

SHOW_RESULT_TIMEOUT = timedelta(seconds=5)
SHOW_OVERSTEPPED_TIMEOUT = timedelta(seconds=5)
DISAPPEARED_TIMEOUT = timedelta(seconds=3)
COUNT_TIMEOUT = timedelta(seconds=1.35)

# states
START, WAITING, PRECOUNT, COUNT3, COUNT2, COUNT1, JUMP, FLYING, LANDED, OVERSTEPPED = "start", "waiting", "precount", "count3", "count2", "count1", "jump", "flying", "landed", "overstepped"
# ground areas
START_AREA, OVERSTEPPED_AREA, LANDING_AREA = "start_area", "overstepped_area", "landing_area"


class Game(object):


    def __init__(self, conf, time_func=datetime.now):
        ga = self.automaton = TimedAutomaton(START, time_func)

        ga.add_state(WAITING)
        ga.add_state(PRECOUNT)
        ga.add_state(COUNT3)
        ga.add_state(COUNT2)
        ga.add_state(COUNT1)
        ga.add_state(JUMP)
        ga.add_state(FLYING)
        ga.add_state(LANDED)
        ga.add_state(OVERSTEPPED)

        ga[START].to(WAITING).on(greater_than_timedelta(timedelta(seconds=.5)))

        ga[WAITING].to(PRECOUNT).on(equal({START_AREA}))
        ga[PRECOUNT].to(COUNT3).on(greater_than_timedelta(COUNT_TIMEOUT))
        ga[COUNT3].to(COUNT2).on(greater_than_timedelta(COUNT_TIMEOUT))
        ga[COUNT2].to(COUNT1).on(greater_than_timedelta(COUNT_TIMEOUT))
        ga[COUNT1].to(JUMP).on(greater_than_timedelta(COUNT_TIMEOUT))
        ga[JUMP].to(FLYING).on(equal(set()))
        ga[JUMP].to(WAITING).on(greater_than_timedelta(DISAPPEARED_TIMEOUT))
        for state in (PRECOUNT, COUNT3, COUNT2, COUNT1):
            ga[state].to(OVERSTEPPED).on(intersect({LANDING_AREA, OVERSTEPPED_AREA}))

        ga[FLYING].to(LANDED).on(equal({LANDING_AREA}))
        ga[JUMP].to(LANDED).on(equal({LANDING_AREA}))
        ga[FLYING].to(OVERSTEPPED).on(intersect({START_AREA, OVERSTEPPED_AREA}))
        ga[FLYING].to(WAITING).on(greater_than_timedelta(DISAPPEARED_TIMEOUT))

        ga[LANDED].to(WAITING).on(greater_than_timedelta(SHOW_RESULT_TIMEOUT))

        ga[OVERSTEPPED].to(WAITING).on(
            greater_than_timedelta(SHOW_OVERSTEPPED_TIMEOUT))

        self.automaton.connect(self.state_changed)

        r = Rect((-1000, -1000), (1, 1))

        # completely off DistanceMeasurer
        self.distance_measurer = DistanceMeasurer(
            r, r.left_line
            )

        gc = self.ground_classifier = RectClassifier()

        if conf is not None:
            rects = {}
            if "ground-areas" in conf:
                for name, rect_def in conf["ground-areas"].iteritems():
                    rects[name] = Rect(rect_def[0], rect_def[1])
                    gc.add_classifier(name, rects[name])

            if "distance-measurement" in conf:
                dmc = conf["distance-measurement"]
                rect = rects[dmc["rect"]]
                line = getattr(rect, dmc["line"])
                self.distance_measurer = DistanceMeasurer(
                    rect,
                    line,
                    )


        self.connections = []
        self.areas_touched = []


    def state_num(self, state):
        return self.automaton.state_num(state)


    def connect(self, connection):
        self.connections.append(connection)


    def emit(self, *message):
        logger.debug("emitting: %r", message)
        for connection in self.connections:
            connection(self, *message)


    def state_changed(self, _automaton, previous, current):
        if current == LANDED and previous != LANDED:
            d = self.distance_measurer.measurement
            if d is None:
                d = 0.2
            self.emit(LANDED, d)
        else:
            if current == JUMP and previous != JUMP:
                # this will triggering the
                # capture of the distance
                # the next time LANDED is reached
                self.distance_measurer.reset()
            self.emit(current)

    def feed(self, points):
        self.areas_touched = self.ground_classifier.classify(points)
        self.distance_measurer.measure(points)
        logger.debug(self.areas_touched)
        self.automaton.feed(self.areas_touched)


    def refeed(self):
        self.automaton.feed(self.areas_touched)
