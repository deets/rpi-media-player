#
import logging

from copy import copy
from math import sqrt

logger = logging.getLogger(__name__)

class Line(object):

    def __init__(self, position, direction):
        self.position = position
        self.direction = direction
        dl = sqrt(direction[0]**2 + direction[1]**2)
        self.normal = self.direction[1] / dl, -self.direction[0] / dl
        self.start = position
        self.end = position[0] + direction[0], position[1] + direction[1]
        if self.start[0] > self.end[0] or self.start[1] > self.end[1]:
            self.start, self.end = self.end, self.start


    def distance(self, point):
        dx, dy = point[0] - self.position[0], point[1] - self.position[1]
        return abs(self.normal[0] * dx + self.normal[1] * dy)


    def __ne__(self, other):
        return not self == other


    def __eq__(self, other):
        return self.start == other.start and self.end == other.end


    def __repr__(self):
        return "<%s (%r - %r)> " % (
            self.__class__.__name__,
            self.start, self.end,
            )


    def __str__(self):
        return repr(self)


class Rect(object):

    def __init__(self, position, size):
        self.position = position
        self.size = size
        self.left = position[0]
        self.right = position[0] + size[0]
        self.bottom = position[1]
        self.top = position[1] + size[1]


    def __contains__(self, point):
        dx, dy = point[0] - self.position[0], point[1] - self.position[1]
        return 0 <= dx < self.size[0] and 0 <= dy < self.size[1]


    @property
    def bottom_line(self):
        return Line(self.position, (self.size[0], 0))


    @property
    def top_line(self):
        return Line(
            (self.position[0], self.position[1] + self.size[1]),
            (self.size[0], 0),
            )


    @property
    def left_line(self):
        return Line(
            self.position,
            (0, self.size[1]),
            )


    @property
    def right_line(self):
        return Line(
            (self.position[0] + self.size[0], self.position[1]),
            (0, self.size[1]),
            )


    def fast_clip(self, points):
        left, right = self.left, self.right
        top, bottom = self.top, self.bottom
        return [(x, y) for x, y in points
                      if left <= x < right and bottom <= y < top]



    def __repr__(self):
        return "<%s %r>" % (
            self.__class__.__name__,
            (self.position, self.size),
            )


class RectClassifier(object):

    def __init__(self):
        self.classifiers = {}


    def classify(self, points):
        res = []
        classifiers = copy(self.classifiers)

        for point in points:
            to_be_removed = []
            for name, rect in classifiers.iteritems():
                if point in rect:
                    res.append(name)
                    to_be_removed.append(name)
            for name in to_be_removed:
                del classifiers[name]

            if not classifiers:
                break

        return frozenset(res)


    def add_classifier(self, name, rect):
        self.classifiers[name] = rect



class DistanceMeasurer(object):


    def __init__(self, clip_rect, line, min_distance=0.2):
        self.clip_rect = clip_rect
        self.line = line
        self.measurement = None
        self.min_distance = min_distance


    def measure(self, points):
        if self.measurement is not None:
            return
        points = self.clip_rect.fast_clip(points)
        if points:
            v = min(self.line.distance(p) for p in points)
            if v >= self.min_distance:
                self.measurement = v
                logger.debug("setting measurement to %.2f", self.measurement)

    def reset(self):
        logger.debug("reset")
        self.measurement = None
