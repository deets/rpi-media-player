#
import sys
import os
import time
import logging
import argparse
import json
import socket
import subprocess
from functools import partial
import threading

from .scip import Scanner
from .util import Rect
from .game import Game
from .demo import DemoScanner

logger = logging.getLogger(__name__)

DEBUG_POINTS = []
WATCHDOG_SCRIPT = "/tmp/sprungkraft-watchdog.py"

def process_coords(world_coords):
    logger.info("got %i world_coords", len(world_coords))


def write_coords(clipfunc, outf, world_coords):
    world_coords = clipfunc(world_coords)
    outf.write("#" * 20)
    outf.write("\n")
    json.dump(world_coords, outf)
    outf.write("\n")


def feed_points_to_game(clipfunc, game, points):
    points = clipfunc(points)
    game.feed(points)


def create_udp_sink(sink_config):
    logger.info("UDP-config: %r", sink_config)
    ip, port = sink_config["ip"], sink_config["port"]
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    def sink(game, *args):
        logger.debug("udp sink args %r", args)
        if len(args) == 2:
            length = args[1]
            encoded_length = min(240, int(round(length * 100)) / 4)
            c = chr(encoded_length)
        else:
            state_num = min(254, game.state_num(args[0]) + 241)
            logger.debug("state_num: %i", state_num)
            c = chr(state_num)
        sock.sendto(c, (ip, port))
    return sink


def watchdog(config):
    if config.get("watchdog", "rpi") == "arduino" \
      and "udp-sink" in config:
        ip, port = config["udp-sink"]["ip"], config["udp-sink"]["port"]
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        while True:
            time.sleep(5.0)
            sock.sendto(chr(255), (ip, port))
    else:
        with open(WATCHDOG_SCRIPT, "w") as outf:
            outf.write("""
    with open("/dev/watchdog") as outf:
       pass
    """)

        while True:
            time.sleep(5.0)
            logger.debug("calling watchdog")
            subprocess.call(["sudo", "python2.7", WATCHDOG_SCRIPT])

def excepthook(*args):
    logger.exception("Sprungrakft was killed")


def main():
    sys.excepthook = excepthook
    config = {}
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r", "--record",
        help="Record measurements into filename",
    )
    parser.add_argument(
        "--log-level", choices=["DEBUG", "INFO", "ERROR"], default="INFO",
        )
    parser.add_argument(
        "--no-gui", action="store_true",
        )
    parser.add_argument(
        "--debug", action="store_true",
        )
    parser.add_argument(
        "--no-full-screen", action="store_true",
        )
    parser.add_argument(
        "--assets",
        help="Asset directory",
        default=os.path.join(
            os.path.dirname(__file__),
            "assets",
            ),
        )
    parser.add_argument(
        "-c", "--config",
        default=os.path.expanduser("~/sprungkraft.conf"),
        )
    parser.add_argument(
        "--demo",
        )
    parser.add_argument(
        "--watchdog",
        action="store_true",
        )

    opts = parser.parse_args()

    logging.basicConfig(
        filename="/tmp/sprungkraft.log",
        level=getattr(logging, opts.log_level),
        format="%(levelname)s:%(asctime)s:%(name)s %(message)s",
        )


    try:
        if os.path.exists(opts.config):
            with open(opts.config) as inf:
                config.update(json.load(inf))
    except:
        logger.exception("Error loading config")


    if opts.demo is None:
        device_file = Scanner.locate_scanner_device()
        scanner_config = config.get("scanner", {})
        scanner = Scanner(
            device_file,
            position=scanner_config.get("position", (0, 0)),
            rotation=scanner_config.get("rotation", 0.0),
            )
    else:
        scanner = DemoScanner(opts.demo)


    ground_rects = []

    if "cliprect" in config:
        clipfunc = Rect(*config["cliprect"]).fast_clip
        ground_rects.append(("cliprect", config["cliprect"]))
    else:
        clipfunc = lambda points: points

    if "ground-areas" in config:
        for name, rect_def in config["ground-areas"].iteritems():
            ground_rects.append((name, rect_def))


    if opts.record:
        logger.info("recording data")
        outf = open(opts.record, "w")
        logger.info("device: %s", device_file)
        scanner.add_listener(partial(write_coords, clipfunc, outf))
        scanner.measure()
        while True:
            time.sleep(.5)

    elif opts.no_gui:
        logger.info("starting game")
        game = Game(config)

        def report_game_state(*args):
            logger.info("%r", args)

        game.connect(report_game_state)

        if "udp-sink" in config:
            game.connect(create_udp_sink(config["udp-sink"]))

        scanner.add_listener(partial(feed_points_to_game, clipfunc, game))
        scanner.measure()
        while True:
            time.sleep(.5)

    else:
        from .gui import start, post_game_event

        def debug_retainer(points):
            global DEBUG_POINTS
            DEBUG_POINTS = points

        logger.info("starting game")

        game = Game(config)
        game.connect(post_game_event)
        if "udp-sink" in config:
            game.connect(create_udp_sink(config["udp-sink"]))

        scanner.add_listener(partial(feed_points_to_game, clipfunc, game))
        scanner.add_listener(debug_retainer)
        scanner.add_idle_listener(game.refeed)
        scanner.measure()

        debug_callback = (lambda: (DEBUG_POINTS, ground_rects)) if opts.debug else None
        logger.info(debug_callback)

        if opts.watchdog:
            t = threading.Thread(target=partial(watchdog, config))
            t.setDaemon(True)
            t.start()

        start(
            debug_callback,
            full_screen=not opts.no_full_screen,
            asset_dir=opts.assets,
            )
