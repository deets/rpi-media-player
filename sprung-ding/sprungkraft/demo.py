import time
import threading
import logging

logger = logging.getLogger(__name__)

class DemoScanner(object):

    def __init__(self, demo_file):
        demo_data = []
        with open(demo_file) as inf:
            for line in inf:
                line = line.strip()
                if not line or line.startswith("#"):
                    continue

                timeout, points = line.split(":", 1)
                timeout = float(timeout)
                points = eval(points)
                demo_data.append((timeout, points))

        self.demo_data = demo_data
        self.listeners = []


    def measure(self):
        t = threading.Thread(target=self.run)
        t.setDaemon(True)
        t.start()


    def run(self):
        while True:
            for timeout, points in self.demo_data:
                logger.debug("feeding demo points: %r", points)
                then = time.time()
                while time.time() - then < timeout:
                    for listener in self.listeners:
                        listener(points)
                    time.sleep(.1)
            time.sleep(3.0)


    def add_listener(self, listener):
        self.listeners.append(listener)

    def add_idle_listener(self, listener):
        pass
