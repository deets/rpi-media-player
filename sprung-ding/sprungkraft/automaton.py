import logging

logger = logging.getLogger(__name__)

class AutomatonException(Exception): pass

class UnknownStateError(AutomatonException): pass

class NoToStateError(AutomatonException): pass




class Automaton(object):

    def __init__(self, starting_state):
        self.starting_state = self.state = starting_state
        self.all_states = {}
        self.state_nums = {}
        self.add_state(starting_state)
        self.connections = []


    def add_state(self, state):
        assert state not in self.all_states
        self.all_states[state] = []
        self.state_nums[state] = len(self.state_nums)
        logger.debug(self.state_nums)


    def state_num(self, state):
        return self.state_nums.get(state, -1)


    def connect(self, slot):
        self.connections.append(slot)


    def __contains__(self, state):
        return state in self.all_states


    def __getitem__(self, state):
        if not state in self.all_states:
            raise UnknownStateError

        automaton = self

        class TransitionAdder(object):

            def __init__(self):
                self.to_state = None

            def to(self, to_state):
                if to_state not in automaton.all_states:
                    raise UnknownStateError
                self.to_state = to_state
                return self


            def on(self, predicate):
                if self.to_state is None:
                    raise NoToStateError
                automaton.all_states[state].append((self.to_state, predicate))


        return TransitionAdder()


    def feed(self, *input_):
        for following, predicate in self.all_states[self.state]:
            if predicate(*input_):
                old = self.state
                self.state = following
                for slot in self.connections:
                    slot(self, old, following)
                break


    def dot(self):
        statements = []
        statements.append("%s [style=bold]" % self.starting_state)
        statements.extend(sorted(name for name in self.all_states if name != self.starting_state))
        for from_, edges in self.all_states.iteritems():
            for index, (to, predicate) in enumerate(edges, start=1):
                statements.append("%s -> %s [ label = \"%i: %s\" ]" % (from_, to, index, str(predicate)))
        return """digraph {
          %s
        }""" % ";\n".join(statements + [""])



class TimedAutomaton(Automaton):

    def __init__(self, starting_state, timesource):
        self.timesource = timesource
        self.when = timesource()
        self.dt = None
        super(TimedAutomaton, self).__init__(starting_state)
        # this must be the first transition callback
        # to ensure when is up-to-date
        self.connect(self.update_when)


    def feed(self, *input_):
        self.dt = self.timesource() - self.when
        super(TimedAutomaton, self).feed(*((self.dt,) + input_))


    def update_when(self, *_args):
        assert self.dt is not None
        self.when += self.dt
        self.dt = None
