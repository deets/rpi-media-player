# -*- coding: utf-8 -*-
import os
import pygame
import time
import logging
from functools import partial
from collections import defaultdict

import pygame.draw
from pygame.locals import (
    HWSURFACE,
    DOUBLEBUF,
    QUIT,
    KEYDOWN,
    MOUSEBUTTONDOWN,
    )

logger = logging.getLogger(__name__)

SCREEN_SIZE = (1920, 1080)



SCREEN_W2 = SCREEN_SIZE[0] / 2
SCREEN_H2 = SCREEN_SIZE[1] / 2
RADIUS = 6.0 # m
SCALE_F = SCREEN_SIZE[1] / (RADIUS * 2)


DISTANCE_POS = (SCREEN_W2, 590)
DISTANCE_COLOR = (255, 255, 255)
DISTANCE_SIZE = 340


def transform((x, y)):
    px = int(SCALE_F * x)
    py = int(SCALE_F * -y)
    return px, py


def debug_gui((points, ground_rects), surface):
    for point in points:
        px, py = transform(point)
        surface.set_at((px + SCREEN_W2, py + SCREEN_H2), (0, 255, 0, 255))

    for _name, (origin, size) in ground_rects:
        ox, oy = transform(origin)
        sx, sy = transform(size)
        r = pygame.Rect((ox +SCREEN_W2, oy + SCREEN_H2), (sx, sy))
        pygame.draw.rect(surface, (255, 0, 255, 255), r, width=1)


class AssetManager(object):

    def __init__(self, asset_dir):
        assets = defaultdict(dict)
        for name in os.listdir(asset_dir):
            logger.debug("looking at asset: %r", name)
            fname = os.path.join(asset_dir, name)
            state, kind = os.path.splitext(name)
            assets_for_state = assets[state]
            assert kind not in assets_for_state
            if kind == ".png":
                image = pygame.image.load(fname)
                logger.debug(image)
                image = image.convert()
                def action(image, screen):
                    screen.blit(image, (0, 0))
                assets_for_state[kind] = partial(action, image)
            elif kind == ".wav":
                sound = pygame.mixer.Sound(fname)
                def action(sound, _screen):
                    sound.play()
                assets_for_state[kind] = partial(action, sound)
            else:
                raise Exception("Unknown asset kind: %r" % kind)

        self.assets = assets
        self.currently_shown = None

    def show(self, screen, state):
        logger.debug("showing assets for %r", state)
        if state not in self.assets:
            logger.warning("No assets for state: %r", state)
        if self.currently_shown != state:
            self.currently_shown = state
            for action in self.assets[state].values():
                action(screen)


def render_character(c, font, cache={}):
    if c not in cache:
        cache[c] = font.render(c, True, DISTANCE_COLOR,)
    return cache[c]

def show_distance(screen, font, distance):
    text = ("%.2fm" % distance).replace(".", ",")
    images = [render_character(c, font) for c in text]
    fw = sum(img.get_width() for img in images)
    px, py = DISTANCE_POS
    px -= fw / 2.0
    for img in images:
        screen.blit(img, (int(px), int(py)))
        px += img.get_width()


GAME_EVENT = pygame.USEREVENT + 1


def post_game_event(_game, state, *distance):
    distance = 0 if not distance else distance[0]
    event = pygame.event.Event(
        GAME_EVENT,
        state=state,
        distance=distance,
        )
    pygame.event.post(event)


def start(
        debug_callback=None,
        font_name=None,
        full_screen=True,
        asset_dir=None,
        ):
    #initialize and setup screen
    pygame.mixer.pre_init(chunksize=256, frequency=15000)
    pygame.init()

    pygame.mouse.set_visible(False)
    screen = pygame.display.set_mode(SCREEN_SIZE, HWSURFACE|DOUBLEBUF)
    if full_screen:
        pygame.display.toggle_fullscreen()
    if font_name is None:
        font_name = os.path.join(os.path.dirname(__file__), "font.otf")

    logger.info("Loading font %s", font_name)
    font = pygame.font.Font(font_name, DISTANCE_SIZE)

    assets = AssetManager(asset_dir)

    stopevents = QUIT, #, KEYDOWN, MOUSEBUTTONDOWN
    last_state = None
    while 1:
        then = time.time()
        for e in pygame.event.get():
            if e.type in stopevents:
                return
            elif e.type == GAME_EVENT:
                current_state = e.state
                if current_state != last_state and current_state is not None:
                    last_state = current_state
                    #logger.debug("Going into state: %r", current_state)
                    assets.show(screen, current_state)
                    if current_state == "landed":
                        show_distance(screen, font, e.distance)

        if debug_callback is not None:
            debug_gui(debug_callback(), screen)
        pygame.display.flip()
        #logger.debug("flipper: %.2f", time.time() - then)
