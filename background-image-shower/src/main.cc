#include <memory>
#include "background.hh"
#include <boost/program_options.hpp>

namespace po = boost::program_options;


int main(int argc, char** argv) {
  bcm_host_init();

  po::options_description desc("Background Image Shower options");
  desc.add_options()
    ("help", "produce help message")
    ("bgimage", po::value<std::string>(), "the background image to show")
    ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }


  std::unique_ptr<OMXBackground> background;
  if(vm.count("bgimage")) {
    auto bgimage_name = vm["bgimage"].as<std::string>();
    background = std::unique_ptr<OMXBackground>(new OMXBackground(bgimage_name, -1));
  } else {
    Color black = {0, 0, 0};
    background = std::unique_ptr<OMXBackground>(new OMXBackground(black, -1));
  }
  while(true) {
    sleep(1);
  }
}
