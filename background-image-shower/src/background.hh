#pragma once

#include <sys/types.h>
#include <bcm_host.h>


typedef struct _Color {
  uint8_t red;
  uint8_t green;
  uint8_t blue;
} Color;


class OMXBackground {
 public:
  OMXBackground(const Color color, int layer);
  OMXBackground(const std::string filename, int layer);
  ~OMXBackground();
 private:
  // prevent copying
  OMXBackground(const OMXBackground&);
  OMXBackground& operator=(const OMXBackground&); // no implementation

  void setup();
  void teardown();
  void allocate_image_data();

  void *_image_data;
  int _width;
  int _height;
  int _pitch;
  VC_IMAGE_TYPE_T _image_type;
  int _layer;

  DISPMANX_UPDATE_HANDLE_T    _update;
  DISPMANX_RESOURCE_HANDLE_T  _resource;
  DISPMANX_ELEMENT_HANDLE_T   _element;

};
