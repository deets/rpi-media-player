#include <cassert>
#include <png++/png.hpp>
#include "bcm_host.h"

#include "background.hh"

OMXBackground::OMXBackground(const Color color, int layer)
  : _image_data(0)
  , _image_type(VC_IMAGE_RGB565)
  , _layer(layer)
{
  _width = 1;
  _height = 1;
  allocate_image_data();
  uint16_t color_data = (color.red >> 3) << 11 | (color.green >> 2) << 5 | (color.blue >> 3);
  ((uint16_t*)_image_data)[0] = color_data;
  setup();
}

OMXBackground::OMXBackground(const std::string filename, int layer)
  : _image_data(0)
  , _image_type(VC_IMAGE_RGB565)
  , _layer(layer)
{
  png::image< png::rgba_pixel > image(filename);
  _width = image.get_width();
  _height = image.get_height();
  allocate_image_data();
  memset(_image_data, 0xff, _pitch * _height / 2.0);

  for(int row = 0; row < _height; row++) {
    uint16_t *row_data = (uint16_t*)_image_data + row * (_pitch >> 1);
    for(int col = 0; col < _width; col++) {
      auto p = image.get_pixel(col, row);
      uint16_t color = (p.red >> 3) << 11 | (p.green >> 2) << 5 | (p.blue >> 3);
      row_data[col] = color;
    }
  }
  setup();
}


void OMXBackground::allocate_image_data() {
  _pitch = ALIGN_UP(_width*2, 32);
  const  int aligned_height = ALIGN_UP(_height, 16);
  const int size = _pitch * aligned_height;
  _image_data = malloc(size);
  memset(_image_data, 0, size);
}

void OMXBackground::setup() {
  DISPMANX_DISPLAY_HANDLE_T display;
  DISPMANX_MODEINFO_T info;
  VC_RECT_T dst_rect, src_rect;
  int ret;
  uint32_t vc_image_ptr;
  display = vc_dispmanx_display_open(0);
  assert(display);
  ret = vc_dispmanx_display_get_info(display, &info);
  assert(ret == 0);
  _resource = vc_dispmanx_resource_create(_image_type, _width, _height, &vc_image_ptr );
  assert( _resource );
  vc_dispmanx_rect_set( &dst_rect, 0, 0, _width, _height);

  ret = vc_dispmanx_resource_write_data(_resource, _image_type, _pitch, _image_data, &dst_rect );
  assert(ret == 0);
  vc_dispmanx_rect_set( &src_rect, 0, 0, _width<<16, _height<<16);
  vc_dispmanx_rect_set( &dst_rect, 0, 0, info.width, info.height);

  _update = vc_dispmanx_update_start(0);
  assert(_update);

  _element = vc_dispmanx_element_add(_update, display, _layer, &dst_rect, _resource, &src_rect,
                                    DISPMANX_PROTECTION_NONE, NULL, NULL, (DISPMANX_TRANSFORM_T)0 );
  assert(_element);
  ret = vc_dispmanx_update_submit_sync( _update );
  assert( ret == 0 );
}


void OMXBackground::teardown() {
  int ret;
  ret = vc_dispmanx_element_remove( _update, _element );
  assert( ret == 0 );
  ret = vc_dispmanx_resource_delete(_resource );
  assert( ret == 0 );
  if(_image_data) {
    free(_image_data);
  }
}

OMXBackground::~OMXBackground() {
  teardown();
}
