{
    "variables" : {
        "dependencies" : "log4cpp libpng12",
       },
    "target_defaults" : {
        "default_configuration" : "Debug",
        "cflags" : [
	"-DSTANDALONE",
	"-D__STDC_CONSTANT_MACROS",
	"-D__STDC_LIMIT_MACROS",
	"-DTARGET_POSIX",
	"-D_LINUX -fPIC",
	"-DPIC",
	"-D_REENTRANT",
	"-D_LARGEFILE64_SOURCE",
	"-D_FILE_OFFSET_BITS=64",
	"-DHAVE_LIBOPENMAX=2",
	"-DOMX",
	"-DOMX_SKIP64BIT",
	"-DUSE_EXTERNAL_OMX",
	"-DHAVE_LIBBCM_HOST",
	"-DUSE_EXTERNAL_LIBBCM_HOST",
	"-DUSE_VCHIQ_ARM",
#            "-v",
            "--sysroot=<(sysroot)",
            "-I=/opt/vc/include",
            "-I=/opt/vc/include/interface/vcos/pthreads",
            "-I=/opt/vc/include/interface/vmcs_host/linux",
            '<!@(pkg-config --cflags <(dependencies))',
	    "-std=c++0x",
            ],


        "ldflags": [
            "-v",
            "<!@(pkg-config --libs-only-L --libs-only-other <(dependencies))",
            "--sysroot=<(sysroot)",
            "-L<(sysroot)/opt/vc/lib",
            "-L<(sysroot)/usr/lib",
         ],
        "libraries": [
            # I need this group-thing because of some
            # circular dependencies, apparently
            "-Wl,--start-group",
            "<!@(pkg-config --libs-only-l <(dependencies))",
            "-lrt",
            "-lc",
            "-lpthread",
            "-lbcm_host",
            "-lvcos",
            "-lvchiq_arm",
            "-lboost_program_options",
            "-Wl,--end-group",
        ],
        "configurations" : {
            "Debug" : {
                "cflags" : [
                    "-O0", "-g",
                    ]
                }
            }
    },

    "targets" : [
        {
            "target_name" : "background-image-shower",
            "type" : "executable",
            "include_dirs" : ["src"],
            "libraries" : [],
            "sources" : [
                "src/background.cc",
		"src/main.cc",
                ]
        }
    ]
}
