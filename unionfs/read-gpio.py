import time
import RPi.GPIO as GPIO


UNIONFS_PIN = 3

# use P1 header pin numbering convention
GPIO.setmode(GPIO.BOARD)
GPIO.setup(UNIONFS_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
while True:
    time.sleep(.1)
    print GPIO.input(UNIONFS_PIN)
