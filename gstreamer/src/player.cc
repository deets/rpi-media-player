#include <stdexcept>

#include <log4cpp/Category.hh>

#include "player.hh"

log4cpp::Category& player_logger = log4cpp::Category::getInstance(std::string("player"));


MultiPlayer::MultiPlayer(bool sync)
    : _duration(-1)
{
  // /usr/bin/gst-launch-1.0 -v filesrc location=~/daily-show.mp4 ! qtdemux ! h264parse ! omxh264dec ! eglglessink
  // movie pipeline elements
  _source = gst_element_factory_make("filesrc", "source");
  _demuxer = gst_element_factory_make("qtdemux", "demuxer");
  _parser = gst_element_factory_make("h264parse", "parser");
  _decoder = gst_element_factory_make("omxh264dec", "decoder");

  // bg image pipeline elements

  _img_source = gst_element_factory_make("filesrc", "img_source");
  _img_decoder = gst_element_factory_make("pngdec", "img_decoder");
  _img_freezer = gst_element_factory_make("imagefreeze", "img_freezer");

  // pipeline tail
  _selector = gst_element_factory_make("input-selector", "selector");
  _sink = gst_element_factory_make("eglglessink", "sink");

  _pipeline = gst_pipeline_new("play-pipeline");
  gst_bin_add_many (
      GST_BIN (_pipeline),
      _source,
      _demuxer,
      _parser,
      _decoder,
      
      _img_source,
      _img_decoder,
      _img_freezer,

      _selector,
      _sink,
      NULL);

  // video linking
  // the demuxer has a dynamic
  // pad that gets connected to the parser on availability
  gst_element_link_many(_source, _demuxer, NULL);
  gst_element_link_many(_parser, _decoder, NULL);


  // image linking
  gst_element_link_many(_img_source, _img_decoder, _img_freezer, NULL);


  // setup the input-selection
  {
    // image first
    GstPadTemplate* selector_sink_pad_template = gst_element_class_get_pad_template (GST_ELEMENT_GET_CLASS (_selector), "sink_%u");
    GstPad* selector_img_pad = gst_element_request_pad (_selector, selector_sink_pad_template, NULL, NULL);
    GstPad* img_src_pad = gst_element_get_static_pad(_img_freezer, "src");
    if(gst_pad_link (img_src_pad, selector_img_pad) != GST_PAD_LINK_OK) {
      player_logger << log4cpp::Priority::ERROR << "linking image chain to input selector failed";
      throw std::runtime_error("pipeline broken");
    }
    gst_object_unref (selector_img_pad);
    gst_object_unref (img_src_pad);
  }
  {
    // now movie
    GstPadTemplate* selector_sink_pad_template = gst_element_class_get_pad_template (GST_ELEMENT_GET_CLASS (_selector), "sink_%u");
    GstPad* selector_video_pad = gst_element_request_pad (_selector, selector_sink_pad_template, NULL, NULL);
    GstPad* video_src_pad = gst_element_get_static_pad(_decoder, "src");
    if(gst_pad_link (video_src_pad, selector_video_pad) != GST_PAD_LINK_OK) {
      player_logger << log4cpp::Priority::ERROR << "linking video chain to input selector failed";
      throw std::runtime_error("pipeline broken");
    }
    gst_object_unref (selector_video_pad);
    gst_object_unref (video_src_pad);
  }

  gst_element_link_many(_selector, _sink, NULL);

  // there is only one dynamic pad, the demuxer - connect with the parser on demand

  g_signal_connect (_demuxer, "pad-added", G_CALLBACK (s_on_pad_added), this);

  if(!sync) {
    player_logger << log4cpp::Priority::DEBUG << "sink not synchronized";
    g_object_set(_sink, "sync", sync, NULL);
    g_object_set(_selector, "sync-streams", sync, NULL);
  }
  GstBus* bus = gst_pipeline_get_bus (GST_PIPELINE (_pipeline));
  gst_bus_add_watch (bus, s_bus_callback, this);
  gst_object_unref (bus);
}


void MultiPlayer::set_movie(const std::string& location, bool loop) {
  player_logger << log4cpp::Priority::DEBUG << "set_movie: " << location << ", loop: " << loop;
  gst_element_set_state(_pipeline, GST_STATE_READY);
  
  g_object_set(_source, "location", location.c_str(), NULL);
  _loop = loop;
  _duration = -1;

  GstPad* video_sink_pad = gst_element_get_static_pad (_selector, "sink_1");
  g_object_set(_selector, "active-pad", video_sink_pad, NULL);
  gst_object_unref (video_sink_pad);

  gst_element_set_state(_pipeline, GST_STATE_PLAYING);
}


void MultiPlayer::set_background_image(const std::string& location) {
  player_logger << log4cpp::Priority::DEBUG << "bgimage: " << location;
  _img_location = location;
  if(_img_location != "") {
    show_background_image();
  }
}


void MultiPlayer::show_background_image() {
  gst_element_set_state(_pipeline, GST_STATE_READY);
  g_object_set(_img_source, "location", _img_location.c_str(), NULL);

  GstPad* img_sink_pad = gst_element_get_static_pad (_selector, "sink_0");
  g_object_set(_selector, "active-pad", img_sink_pad, NULL);
  gst_object_unref (img_sink_pad);

  gst_element_set_state(_pipeline, GST_STATE_PLAYING);
}

void MultiPlayer::on_pad_added(GstElement* element, GstPad* pad) {
  player_logger << log4cpp::Priority::DEBUG << "on_pad_added";
  player_logger << log4cpp::Priority::DEBUG << "Dynamic pad created, linking demuxer/parser";

  GstPad *sinkpad;
  sinkpad = gst_element_get_static_pad (_parser, "sink");
  gst_pad_link (pad, sinkpad);
  gst_object_unref (sinkpad);
}


void MultiPlayer::s_on_pad_added(
    GstElement *element,
    GstPad     *pad,
    gpointer    data)
{
  MultiPlayer* player = static_cast<MultiPlayer*>(data);
  player->on_pad_added(element, pad);
}


int MultiPlayer::timer_callback() {
  gint64 pos, len;
  
  if (gst_element_query_position (_pipeline, GST_FORMAT_TIME, &pos)
      && gst_element_query_duration (_pipeline, GST_FORMAT_TIME, &len)) {
  }
  return true;
}


int MultiPlayer::s_timer_callback(gpointer data) {
  MultiPlayer* player = static_cast<MultiPlayer*>(data);
  return player->timer_callback();
}


int MultiPlayer::s_bus_callback(GstBus* bus, GstMessage* msg, gpointer data) {
  MultiPlayer* player = static_cast<MultiPlayer*>(data);
  return player->bus_callback(bus, msg);
}


void MultiPlayer::setup_loop_segment() {
  // It seems the gst_event_new_seek
  // produces an GST_MESSAGE_ASYNC_DONE event
  // so without this guard, we would forever seek
  // to the beginning of the movie
  if(_duration == -1) {
    GstEvent* seek_event;
    gst_element_query_duration(_pipeline, GST_FORMAT_TIME, &_duration);
    player_logger << log4cpp::Priority::DEBUG << "duration: " << _duration;
    seek_event = gst_event_new_seek (1.0, GST_FORMAT_TIME,
				     (GstSeekFlags) (GST_SEEK_FLAG_SEGMENT | GST_SEEK_FLAG_FLUSH),
				     GST_SEEK_TYPE_SET, 0,
				     GST_SEEK_TYPE_SET, _duration);
    gst_element_send_event(_pipeline, seek_event);
  }
}


int MultiPlayer::bus_callback(GstBus *bus, GstMessage *msg) {
  switch (GST_MESSAGE_TYPE(msg)) {
  case GST_MESSAGE_ASYNC_DONE:
    player_logger << log4cpp::Priority::DEBUG << "GST_MESSAGE_ASYNC_DONE";
    if(_loop) {
      player_logger << log4cpp::Priority::DEBUG << "looping requested\n";
      setup_loop_segment();
    }
    break;
  case GST_MESSAGE_SEGMENT_DONE:
    player_logger << log4cpp::Priority::DEBUG << "GST_MESSAGE_SEGMENT_DONE";
    if (!gst_element_seek(
	    _pipeline, 1.0, GST_FORMAT_TIME,
	    (GstSeekFlags) (GST_SEEK_FLAG_SEGMENT | GST_SEEK_FLAG_FLUSH),
	    GST_SEEK_TYPE_SET, 0,
	    GST_SEEK_TYPE_SET, _duration)) {
      g_print("Seek failed!\n");
      }
    setup_loop_segment();
    break;
  case GST_MESSAGE_EOS:
    player_logger << log4cpp::Priority::DEBUG << "GST_MESSAGE_EOS";    
    show_background_image();
    break;
  case GST_MESSAGE_STATE_CHANGED:
    player_logger << log4cpp::Priority::DEBUG << "GST_MESSAGE_STATE_CHANGED";    
    GstState old_state, new_state, pending_state;
    gst_message_parse_state_changed (msg, &old_state, &new_state, &pending_state);
    player_logger << log4cpp::Priority::DEBUG << GST_MESSAGE_SRC_NAME (msg) << " state changed from "
		  << gst_element_state_get_name (old_state) << " to " <<  gst_element_state_get_name (new_state);
    break;
  default:
    player_logger << log4cpp::Priority::DEBUG << "unknown message: " << GST_MESSAGE_TYPE_NAME(msg);
    break;
  }
  return true;
}

void MultiPlayer::play() {
  GMainLoop *loop;
  g_timeout_add_seconds(1, s_timer_callback, this);
  loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (loop);
}

MultiPlayer::~MultiPlayer() {
  gst_element_set_state (_pipeline, GST_STATE_NULL);
    gst_object_unref (_pipeline);
}
