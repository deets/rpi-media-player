#include <iostream>
#include <unistd.h>

#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>
#include <zmq.hpp>

#include "common.hh"
#include "ipc.hh"


log4cpp::Category& logger = log4cpp::Category::getInstance(std::string("ipc"));

DBusConnector::DBusConnector(Callback callback) 
  : _callback(callback)
{
}


DBusConnector::~DBusConnector() {

}

void DBusConnector::run() {
  logger.info("running zmq thread\n");
  zmq::context_t context (1);
  zmq::socket_t socket (context, ZMQ_REP);
  socket.bind ("tcp://*:5555");
  while (true) {
    zmq::message_t request;

    // Wait for next request from client
    socket.recv (&request);
    logger << log4cpp::Priority::DEBUG << "Received " << request.size() << "bytes";
    std::string input((char*)request.data(), request.size());
    _callback(input);
    // Send reply back to client
    zmq::message_t reply (2);
    memcpy ((void *) reply.data (), "OK", 2);
    socket.send (reply);
  }  
}


void* DBusConnector_run(gpointer data) {
  DBusConnector* connector = static_cast<DBusConnector*>(data);
  connector->run();
  return NULL;
}


void DBusConnector::start() {
  GError* error = NULL;
  _thread = g_thread_create(DBusConnector_run, this, false, &error);
  if(error) {
    report_error_and_terminate("Couldn't create thread", error);
  }
}
