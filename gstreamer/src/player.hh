#pragma once
#include <string>
#include <iostream>
#include <gst/gst.h>


class MultiPlayer {
  GstElement* _pipeline;
  GstElement* _source;
  GstElement* _demuxer;
  GstElement* _parser;
  GstElement* _decoder;

  GstElement* _img_source;
  GstElement* _img_decoder;
  GstElement* _img_freezer;

  GstElement* _selector;
  GstElement* _sink;

  bool _loop;
  gint64 _duration;

  std::string _img_location;

public:
  MultiPlayer(bool sync);
  virtual ~MultiPlayer();

  void play();
  void set_movie(const std::string& location, bool loop);
  void set_background_image(const std::string& location);

private:
  void show_background_image();
  void on_pad_added(GstElement* element, GstPad* pad);
  static void s_on_pad_added(
      GstElement *element,
      GstPad     *pad,
      gpointer    data);
  int timer_callback();
  static int s_timer_callback(gpointer data);
  static int s_bus_callback(GstBus* bus, GstMessage* msg, gpointer data);
  void setup_loop_segment();
  int bus_callback(GstBus *bus, GstMessage *msg);
};
