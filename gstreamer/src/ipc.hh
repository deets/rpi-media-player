#pragma once
#include <boost/function.hpp>
#include <glib/gthread.h>
#include "protobuf/rpi-player.pb.h"

class DBusConnector {

public:
  typedef boost::function<void(const std::string&)> Callback;
  DBusConnector(Callback callback);
  virtual ~DBusConnector();

  void run();
  void start();

private:
  GThread* _thread;
  Callback _callback;
};
