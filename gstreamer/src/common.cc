#include <iostream>
#include "common.hh"
#include <stdlib.h>


void report_error_and_terminate(const char* message, GError* error) {
  std::cerr << message << "\n" << error->message << "\n";
  exit(1);
}
