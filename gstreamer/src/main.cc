#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdio.h>

#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>

#include <boost/bind.hpp>
#include <boost/program_options.hpp>

#include <interface/vmcs_host/vc_dispmanx.h>
#include <bcm_host.h>

#include "protobuf/rpi-player.pb.h"
#include "ipc.hh"
#include "player.hh"

namespace po = boost::program_options;

log4cpp::Category& main_logger = log4cpp::Category::getInstance(std::string("main"));

void set_movie(MultiPlayer* player, const std::string& input) {
  main_logger << log4cpp::Priority::DEBUG << "set_movie: " << input;
  rpiplayer::PlayMovie msg;
  if(msg.ParseFromString(input)) {
    player->set_movie(msg.uri(), msg.loop());
  }
}

// This is blatantly ripped from OMXPlayer - thus I added the same license,
// and very many thanks to the great work :)
static void blank_background()
{
  // we create a 1x1 black pixel image that is added to display just behind video
  DISPMANX_DISPLAY_HANDLE_T display;
  DISPMANX_UPDATE_HANDLE_T update;
  DISPMANX_RESOURCE_HANDLE_T resource;
  DISPMANX_ELEMENT_HANDLE_T element;
  int ret;
  uint32_t vc_image_ptr;
  VC_IMAGE_TYPE_T type = VC_IMAGE_RGB565;
  uint16_t image = 0x0000; // black
  int layer = 0 - 1;

  VC_RECT_T dst_rect, src_rect;
  bcm_host_init();
  display = vc_dispmanx_display_open(0);
  assert(display);

  resource = vc_dispmanx_resource_create( type, 1 /*width*/, 1 /*height*/, &vc_image_ptr );
  assert( resource );

  vc_dispmanx_rect_set( &dst_rect, 0, 0, 1, 1);

  ret = vc_dispmanx_resource_write_data( resource, type, sizeof(image), &image, &dst_rect );
  assert(ret == 0);

  vc_dispmanx_rect_set( &src_rect, 0, 0, 1<<16, 1<<16);
  vc_dispmanx_rect_set( &dst_rect, 0, 0, 0, 0);

  update = vc_dispmanx_update_start(0);
  assert(update);

  element = vc_dispmanx_element_add(update, display, layer, &dst_rect, resource, &src_rect,
                                    DISPMANX_PROTECTION_NONE, NULL, NULL, (DISPMANX_TRANSFORM_T)0 );
  assert(element);

  ret = vc_dispmanx_update_submit_sync( update );
  assert( ret == 0 );
}

int main(int argc, char** argv) {
  gst_init (&argc, &argv);
  g_thread_init(NULL);

  po::options_description desc("RPI Player options");
  desc.add_options()
    ("help", "produce help message")
    ("no-sync", "run sink without sync")
    ("logging", po::value<std::string>(), "log4cpp properties file")
    ("bgimage", po::value<std::string>(), "the background image to show")
    ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  bool sync = vm.count("no-sync") == 0;

  if(vm.count("logging")) {
    std::string log_props = vm["logging"].as<std::string>();
    log4cpp::PropertyConfigurator::configure(log_props);
  }

  if(vm.count("bgimage") == 0) {
    std::cerr << "The bgimage-parameter is required!\n";
    return 1;
  }

  blank_background();
  {
    MultiPlayer player(sync);
    player.set_background_image(vm["bgimage"].as<std::string>());

    DBusConnector::Callback movie_setter = boost::bind(
      set_movie,
      &player,
      _1
    );
    DBusConnector connector(movie_setter);
    connector.start();
    player.play();
  }
}
