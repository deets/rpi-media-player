{
    "variables" : {
        "dependencies" : "gstreamer-1.0 protobuf libzmq glib-2.0 gthread-2.0 log4cpp",
       },
    "target_defaults" : {
        "default_configuration" : "Debug",
        "cflags" : [
#            "-v",
            "--sysroot=<(sysroot)",
            "-I=/opt/vc/include",
            "-I=/opt/vc/include/interface/vcos/pthreads",
            "-I=/opt/vc/include/interface/vmcs_host/linux",
            '<!@(pkg-config --cflags <(dependencies))',
            ],

        #        "cxxflags" : ["--sysroot", "<(sysroot)"],
        "ldflags": [
            "-v",
            "<!@(pkg-config --libs-only-L --libs-only-other <(dependencies))",
            "--sysroot=<(sysroot)",
            "-L<(sysroot)/opt/vc/lib",
            "-L<(sysroot)/usr/lib",
         ],
        "libraries": [
            # I need this group-thing because of some
            # circular dependencies, apparently
            "-Wl,--start-group",
            "<!@(pkg-config --libs-only-l <(dependencies))",
            "-lpthread",
            "-lpcre",
            "-lrt",
            "-lc",
            "-lbcm_host",
            "-lvcos",
            "-lvchiq_arm",
            "-lboost_program_options",
            "-Wl,--end-group",
        ],
        "configurations" : {
            "Debug" : {
                "cflags" : [
                    "-O0", "-g",
                    ]
                }
            }
    },

    "targets" : [
        {
            "target_name" : "rpi-player",
            "type" : "executable",
            "include_dirs" : ["."],
            "libraries" : [],
            "sources" : [
                "protobuf/rpi-player.pb.cc",
                "src/main.cc",
                "src/common.cc",
                "src/ipc.cc",
                "src/player.cc",
                ]
        }
    ]
}
