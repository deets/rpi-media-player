#!/usr/bin/env python2.7
import sys
import time
import syslog


import zmq
import RPi.GPIO as GPIO

from rpi_player_pb2 import PlayMovie

syslog.openlog("rpi-player-movie-trigger")

def log(message):
    syslog.syslog(message)
    print message

UNIONFS_PIN = 7


def play_movie(_channel):
    log("starting movie: %s" % sys.argv[1])
    pm = PlayMovie()
    pm.uri = sys.argv[1]
    pm.loop = False
    message = pm.SerializeToString()
    context = zmq.Context()
    player = context.socket (zmq.REQ)
    player.connect ("tcp://*:5555")
    player.send(message)
    log("play_movie finished")


# use P1 header pin numbering convention
GPIO.setmode(GPIO.BOARD)
# pull down
GPIO.setup(UNIONFS_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

GPIO.add_event_detect(UNIONFS_PIN, GPIO.FALLING, callback=play_movie, bouncetime=300)

while True:
    time.sleep(10.0)
