#!/usr/bin/env python2.7
import sys
import zmq
from rpi_player_pb2 import PlayMovie


pm = PlayMovie()
pm.uri = sys.argv[1]
pm.loop = False

message = pm.SerializeToString()

context = zmq.Context()

player = context.socket (zmq.REQ)
player.connect ("tcp://*:5555")
player.send(message)
