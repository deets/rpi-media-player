export RPI_TOOLS=~/rpi/tools
export RPI_ROOT=~/rpi/root

export SYSROOT=$RPI_ROOT

export PKG_CONFIG_PATH=$RPI_ROOT/usr/lib/arm-linux-gnueabihf/pkgconfig:$RPI_ROOT/usr/share/pkgconfig:$RPI_ROOT/usr/lib/pkgconfig:$RPI_ROOT/usr/local/lib/pgkconfig:$RPI_ROOT/usr/local/lib/pkgconfig
export PKG_CONFIG_SYSROOT_DIR=$SYSROOT

export CC=$RPI_TOOLS/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-gcc
export CXX=$RPI_TOOLS/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-g++
export AR=$RPI_TOOLS/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-ar
export LD=$CXX

