#!/usr/bin/env python2.7
import os
import time
import sys
import subprocess
import syslog

j = os.path.join
ex = os.path.exists

syslog.openlog("rpi-player-function-selector")

os.chdir("/home/pi")

def log(message):
    syslog.syslog(message)
    print message

dry_run = "-n" in sys.argv

def popen(*args, **kwargs):
    log("popen: %r" % ((args, kwargs),))
    if not dry_run:
        return subprocess.Popen(*args, **kwargs)



def select_function():
    if has_scanner:
        log("Starting Sprungkraft")
        cmd = ["/home/pi/pypy-2.3-linux-armhf-raspbian/bin/sprungkraft", "--watchdog", "--assets", "/home/pi/sprungkraft-assets"]
        if has_usbstick:
            cmd.append("--debug")
        popen(cmd)
        return True
    elif has_usbstick:
        for candidate in os.listdir("/media"):
            log("Checking %s" % candidate)
            root = os.path.join("/media", candidate)
            log("\n".join(os.listdir(root)))
            if ex(j(root, "background.png")) and ex(j(root, "movie.h264")):
                log("Starting triggered movie playback")
                popen(["/home/pi/bin/background-image-shower", "--bgimage", j(root, "background.png")])
                popen(["sudo", sys.executable, "bin/trigger-movie.py", j(root, "movie.h264")])
                return True
            elif ex(j(root, "movie.h264")):
                log("Starting endless loop movie playback")
                # clear background
                popen(["/home/pi/bin/background-image-shower"])
                # play endless loop
                popen(["sudo", "/home/pi/bin/loop-player", "-w", "-l", j(root, "movie.h264")])
                return True


def check_capabilities():
    has_usbstick = bool(os.listdir("/media"))
    if os.path.exists("/dev/serial/by-id"):
        has_scanner = bool([candidate for candidate in os.listdir("/dev/serial/by-id") if "URG" in candidate])
    else:
        has_scanner = False

    return has_usbstick, has_scanner


log("RPI Player Function Selector")

for _ in xrange(200):
    try:
        has_usbstick, has_scanner = check_capabilities()
        log(("no " if not has_usbstick else "") + "usbstick found")
        log(("no " if not has_scanner else "") + "laser scanner found")
        if select_function():
            break

    except:
        log("exception")
        log(str(sys.exc_info()[1]))

    log("no function started, waiting!")
    time.sleep(1.0)
