#!/usr/bin/env python2.7
import os
import sys
import time
import syslog
import subprocess
import threading


import RPi.GPIO as GPIO

TIMEOUT = 5.0
UNIONFS_PIN = 7

TIMESTAMP = time.time() - TIMEOUT
MOVIE = sys.argv[1]

syslog.openlog("rpi-player-movie-trigger OMX")

def log(message):
    syslog.syslog(message)
    print message


def watchdog():
    while True:
        time.sleep(5.0)
        with open("/dev/watchdog") as inf:
            pass


def play_movie(_channel):
    global TIMESTAMP
    now = time.time()
    if now - TIMESTAMP > TIMEOUT:
        log("starting movie: %s" % MOVIE)
        p = subprocess.Popen(["/home/pi/bin/loop-player", MOVIE])
        p.communicate()
        TIMESTAMP = now
    else:
        log("suppressed next triggering")


# use P1 header pin numbering convention
GPIO.setmode(GPIO.BOARD)
# pull down
GPIO.setup(UNIONFS_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

GPIO.add_event_detect(UNIONFS_PIN, GPIO.FALLING, callback=play_movie, bouncetime=300)


t = threading.Thread(target=watchdog)
t.setDaemon(True)
t.start()

while True:
    time.sleep(10.0)
